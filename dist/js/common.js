// Document READY
$(document).ready(function(){
	$('input[name="phone"]').mask("+7(999)999-99-99",{autoclear: false});

	// Input
	$('.feedback__lb-input input').on('focusout', function(){
		var val = $(this).val();
			span = $(this).find('~span');

		val ? span.addClass('active') : span.removeClass('active');	

		if ( val == "+7(___)___-__-__" ) span.removeClass('active');
	});

	// Switch
	(function(){

		var tl = new TimelineMax();

		$('.switch-label').click(function(){
			var th = $(this);

			var wrap = th.closest('.switch'),
				line = wrap.find('.switch-line'),
				label = wrap.find('.switch-label');
			
			if ( th.hasClass('active') ) return;

			var leftVal = 3;
			
			if ( th.attr('for') == 'partic-online' || th.attr('for') == 'm-partic-online' ) {
				leftVal = (wrap.width() / 2) - 4;
			}

			tl.to(label, .15, {opacity: 0})
			.addCallback(function(){
				label.removeClass('active');
				th.addClass('active');

				line.addClass('active');
			})
			.to(line, .1, { top: 13, bottom: 13 })
			.to(line, .2, { left: leftVal }, "-=.1")
			.to(line, .1, { top: 3, bottom: 3 }, "-=.1")
			.addCallback(function(){
				line.removeClass('active');
			})
			.to(label, .15, {opacity: 1})

		});
	}());

	// Youtube
	$('a[href^="#"]').click(function(e){
		e.preventDefault();
		
		var href = $(this).attr('href'),
			src = $(href).find('iframe').attr('src');
		
		if ( href == "#" ) return;

		if ( $(href).length ) {
			$.fancybox.open({
				src  : href,
				type : 'inline',
				touch: {
					vertical: false, // Allow to drag content vertically
					momentum: true // Continue movement after releasing mouse/touch when panning
				},
				afterLoad : function() {
					if ( href == '#youtube' ) {
						$('#youtube-iframe').attr('src', src + "&autoplay=1;showinfo=0");
					}
				},
				beforeClose: function() {
					if ( href == '#youtube' ) {
						$('#youtube-iframe').attr('src', src);
					}
				}
			});
		}
	});

	// Map
	ymaps.ready(function(){
		var ymap = new ymaps.Map("map", {
			center: [55.783600, 37.719244],
			zoom: 14,
			controls: []
		});
		
		var mark = new ymaps.Placemark([55.783600, 37.719244], {
			hintContent: 'м.Семеновская',
			balloonContent: 'Москва, Измайловский Вал, дом 2, корпус 1'
		});
		
		ymap.geoObjects.add(mark);

	});
});

// WINDOW LOAD
$(window).on('load',function(){

});